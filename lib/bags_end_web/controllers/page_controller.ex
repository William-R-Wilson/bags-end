defmodule BagsEndWeb.PageController do
  use BagsEndWeb, :controller
  alias BagsEnd.Chats

  def index(conn, _params) do
    current_user = current_user(conn) 
    messages = Chats.list_messages()
    render(conn, "index.html", messages: messages, current_user: current_user)
  end
end
