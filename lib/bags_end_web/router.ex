defmodule BagsEndWeb.Router do
  use BagsEndWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", BagsEndWeb do
    pipe_through :browser
    
    get "/sign-in", SessionController, :new
    post "/sign-in", SessionController, :create
    delete "sign-out", SessionController, :delete
    get "/", PageController, :index
    resources "/registrations", UserController, only: [:create, :new]
  end

  # Other scopes may use custom stacks.
  # scope "/api", BagsEndWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: BagsEndWeb.Telemetry
    end
  end
end
