defmodule BagsEnd.Repo do
  use Ecto.Repo,
    otp_app: :bags_end,
    adapter: Ecto.Adapters.Postgres
end
