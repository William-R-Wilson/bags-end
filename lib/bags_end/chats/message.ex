defmodule BagsEnd.Chats.Message do
  use Ecto.Schema
  import Ecto.Changeset

  alias BagsEnd.Chats.Message

  schema "messages" do
    field :body, :string
    field :name, :string
    field :user_id, :integer
    timestamps()
  end

  @doc false
  def changeset(message, attrs) do
    message
    |> cast(attrs, [:name, :body, :user_id])
    |> validate_required([:name, :body, :user_id])
  end
end
