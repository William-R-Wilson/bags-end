let WaterCooler = {
  init(socket) {
    let channel = socket.channel('water_cooler:lobby', {})
    channel.join()
    this.listenForChats(channel)
  },

  listenForChats(channel) {
    let user = document.getElementById('user')
    let userName = user.dataset.name
    let userId = user.dataset.id

    document.getElementById('chat-form').addEventListener('submit', function(e){
      e.preventDefault()

      let userMsg = document.getElementById('user-msg').value

      channel.push('shout', {name: userName, body: userMsg, user_id: userId })

      document.getElementById('user-msg').value = ''
    })

    document.getElementById('chat-form').addEventListener('keydown', function(e) {
      let userMsg = document.getElementById('user-msg').value
      if (e.keyCode === 13 && userMsg != "") {
        channel.push('shout', { name: userName, body: userMsg, user_id: userId } )
        document.getElementById('user-msg').value = ''
      }
      else {
        return false
      }
    })

// dice roller
    let dice = document.getElementsByClassName("die");
    Array.from(dice).forEach(function(element) {
      element.addEventListener('click', function(){
        let timesEl = document.getElementById("roller-times");
        let times = timesEl.value;
        let die = this.dataset.die;
        let msg = `rolled ${times}d${die} with a result of ${roll(times, die)} for ${userName}`;
        channel.push('shout', { name: "Dice Roller Bot", body: msg, user_id: userId } )
      });
    });

    function roll(times, die) {
      let sum = 0;
      for (var i=0; i<times; i++) {
        sum += Math.ceil(Math.random() * Math.ceil(die))
      }
      return sum;
    }

    channel.on('shout', payload => {
      let chatBox = document.querySelector('#chat-box')

      let msgBlock = `<p><strong>${payload.name}</strong>: ${payload.body}</p>`
      chatBox.insertAdjacentHTML('afterbegin', msgBlock)
    })
  }
}

export default WaterCooler
