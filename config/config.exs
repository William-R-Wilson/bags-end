# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :bags_end,
  ecto_repos: [BagsEnd.Repo]

# Configures the endpoint
config :bags_end, BagsEndWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "oIAvCLDdY8OyrMTchfYX0Xi0z1aawAlj0uo4zPb7dxIiIoNHorP200+RP5b0h6Ox",
  render_errors: [view: BagsEndWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: BagsEnd.PubSub,
  live_view: [signing_salt: "gPgsEC2H"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
